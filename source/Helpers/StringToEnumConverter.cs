﻿using System.Linq;
using System.Text.Transformation;
using Newtonsoft.Json;

namespace System.Net.Push.Gcm.Helpers
{
	internal class StringToEnumConverter : JsonConverter
	{
		public CaseType DataStyle { get; internal set; }
		public CaseType CodeStyle { get; internal set; }

		public StringToEnumConverter(CaseType dataStyle = CaseType.CamelCase, CaseType codeStyle = CaseType.PascalCase)
		{
			DataStyle = dataStyle;
			CodeStyle = codeStyle;
		}

		public override bool CanConvert(Type objectType)
		{
			return objectType.IsEnum;
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (!objectType.IsEnum)
			{
				return null;
			}

			object defaultEnumValue = Enum.Parse(objectType, Enum.GetNames(objectType).First());
			string name = reader.ReadAsString().ToCase(CodeStyle);

			return Enum.GetNames(objectType).Contains(name)
				? Enum.Parse(objectType, name)
				: defaultEnumValue
			;
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if (value != null && value.GetType().IsEnum)
			{
				writer.WriteValue(value.ToString().ToCase(DataStyle));
			}
		}

	}
}
