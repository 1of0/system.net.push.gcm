﻿using Newtonsoft.Json;

namespace System.Net.Push.Gcm.Helpers
{
	public static class Json
	{
		private static readonly JsonSerializerSettings JsonSettings = new JsonSerializerSettings
		{
			ContractResolver = new SnakeCaseContractResolver()
		};

		public static T Deserialize<T>(string json)
		{
			return JsonConvert.DeserializeObject<T>(json, JsonSettings);
		}

		public static string Serialize<T>(T obj)
		{
			return JsonConvert.SerializeObject(obj, JsonSettings);
		}
	}
}