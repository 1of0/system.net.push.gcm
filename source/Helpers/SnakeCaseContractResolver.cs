﻿using System.Text.Transformation;
using Newtonsoft.Json.Serialization;

namespace System.Net.Push.Gcm.Helpers
{
	public class SnakeCaseContractResolver : DefaultContractResolver
	{
		public SnakeCaseContractResolver()
			: base(true)
		{
		}

		protected override string ResolvePropertyName(string propertyName)
		{
			return propertyName.ToSnakeCase();
		}
	}
}