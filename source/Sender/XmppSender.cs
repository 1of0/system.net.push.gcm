﻿using Sharp.Xmpp.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Net.Push.Gcm.Sender
{
	public class XmppSender : IDisposable
	{
		public const string GoogleServerProduction = "gcm.googleapis.com:5235";
		public const string GoogleServerPreProduction = "gcm.googleapis.com:5236";

		private Sharp.Xmpp.Client.XmppClient _xmpp;

		public string Server { get; set; }

		public XmppSender(string server = GoogleServerProduction)
		{
			//_xmpp = new XmppClient();
		}

		public void Dispose()
		{
			if (_xmpp != null)
			{
				_xmpp.Dispose();
			}
		}
	}
}
