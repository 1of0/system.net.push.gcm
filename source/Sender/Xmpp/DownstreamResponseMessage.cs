﻿using System.Net.Push.Gcm.Helpers;
using System.Text.Transformation;
using Newtonsoft.Json;

namespace System.Net.Push.Gcm.Sender.Xmpp
{
	public class DownstreamResponseMessage
	{
		public string From { get; set; }

		public string MessageId { get; set; }

		[JsonConverter(typeof(StringToEnumConverter), CaseType.SnakeCase)]
		public DownstreamResponseMessageType MessageType { get; set; }

		[JsonConverter(typeof(StringToEnumConverter), CaseType.UpperSnakeCase)]
		public DownstreamResponseMessageError Error { get; set; }

		public string ErrorDescription { get; set; }
	}

	public enum DownstreamResponseMessageType
	{
		Ack,
		Nack
	}

	public enum DownstreamResponseMessageError
	{
		InvalidJson,
		BadRegistration,
		DeviceUnregistered,
		BadAck,
		ServiceUnavailable,
		InternalServerError,
		DeviceMessageRateExceeded,
		ConnectionDraining
	}
}
