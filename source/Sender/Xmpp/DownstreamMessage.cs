﻿
namespace System.Net.Push.Gcm.Sender.Xmpp
{
	public class DownstreamMessage<T>
	{
		public string To { get; set; }

		public string MessageId { get; set; }

		public string CollapseKey { get; set; }

		public bool DelayWhileIdle { get; set; }

		public int TimeToLive { get; set; }

		public bool DeliveryReceiptRequested { get; set; }

		public T Data { get; set; }
	}
}
