﻿
namespace System.Net.Push.Gcm.Sender.Xmpp
{
	public class UpstreamMessage<T>
	{
		public string From { get; set; }

		public string Category { get; set; }

		public string MessageId { get; set; }

		public T Data { get; set; }
	}
}
