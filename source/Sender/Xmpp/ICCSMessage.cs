﻿
namespace System.Net.Push.Gcm.Sender.Xmpp
{
	public interface ICcsMessage
	{
		CcsMessageType MessageType { get; }
	}

	public enum CcsMessageType
	{
		Receipt,
		Control
	}
}