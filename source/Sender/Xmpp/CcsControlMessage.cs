﻿using System.Net.Push.Gcm.Helpers;
using System.Text.Transformation;
using Newtonsoft.Json;

namespace System.Net.Push.Gcm.Sender.Xmpp
{
	public class CcsControlMessage : ICcsMessage
	{
		[JsonConverter(typeof(StringToEnumConverter), CaseType.SnakeCase)]
		public CcsMessageType MessageType { get; set; }

		[JsonConverter(typeof(StringToEnumConverter), CaseType.UpperSnakeCase)]
		public CcsControlType ControlType { get; set; }

	}

	public enum CcsControlType
	{
		ConnectionDraining
	}
}