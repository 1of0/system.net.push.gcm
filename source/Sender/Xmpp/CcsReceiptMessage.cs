﻿using System.Net.Push.Gcm.Helpers;
using System.Text.Transformation;
using Newtonsoft.Json;

namespace System.Net.Push.Gcm.Sender.Xmpp
{
	public class CcsReceiptMessage : ICcsMessage
	{
		[JsonConverter(typeof(StringToEnumConverter), CaseType.SnakeCase)]
		public CcsMessageType MessageType { get; set; }

		public string From { get; set; }

		public string MessageId { get; set; }

		public string Category { get; set; }

		public ReceiptData Data { get; set; }

		public class ReceiptData
		{
			[JsonConverter(typeof(StringToEnumConverter), CaseType.UpperSnakeCase)]
			public CcsReceiptMessageStatus MessageStatus { get; set; }

			public string OriginalMessageId { get; set; }

			public string DeviceRegistrationId { get; set; }
		}
	}

	public enum CcsReceiptMessageStatus
	{
		MessageSentToDevice
	}
}